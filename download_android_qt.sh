#!/bin/sh

mkdir -p /opt/Qt
cd /opt/Qt

QT_SERVER=http://download.qt.io
ANDROID_DIR=online/qtsdkrepository/linux_x64/android
QT_VERSION_DIR=qt5_5141

QT_URL=$QT_SERVER/$ANDROID_DIR/$QT_VERSION_DIR

FILE=5.14.1-0-202001241028qtpurchasing-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5141.qtpurchasing.android/$FILE
p7zip -d $FILE

FILE=5.14.1-0-202001241028qtnetworkauth-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5141.qtnetworkauth.android/$FILE
p7zip -d $FILE

FILE=5.14.1-0-202001241028qtdatavis3d-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5141.qtdatavis3d.android/$FILE
p7zip -d $FILE

FILE=5.14.1-0-202001241028qtcharts-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
wget -q $QT_URL/qt.qt5.5141.qtcharts.android/$FILE
p7zip -d $FILE

FILES="5.14.1-0-202001241028qtxmlpatterns-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtwebview-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtwebsockets-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtwebchannel-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qttranslations-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qttools-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtsvg-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtspeech-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtserialport-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtsensors-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtscxml-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtquickcontrols2-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtquickcontrols-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtmultimedia-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtlocation-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtimageformats-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtgraphicaleffects-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtgamepad-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtdeclarative-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtconnectivity-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtbase-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qtandroidextras-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z
5.14.1-0-202001241028qt3d-Linux-RHEL_7_6-Clang-Android-Android_ANY-Multi.7z"
for FILE in $FILES
do
    wget -q $QT_URL/qt.qt5.5141.android/$FILE
    p7zip -d $FILE
done

# RPath etc. is set to /home/qt/work/install/lib - so doing a symlink
mkdir -p /home/qt/work
ln -s /opt/Qt/5.14.1/android /home/qt/work/install

# switch Qt license to OSS
#/opt/Qt/5.14.1/android_armv7/mkspecs/qconfig.pri
#QT_EDITION = Enterprise -> OpenSource
PRI_FILE="/opt/Qt/5.14.1/android/mkspecs/qconfig.pri"
sed -i 's/Enterprise/OpenSource/g' "${PRI_FILE}"
sed -i 's/licheck.*//g' "${PRI_FILE}"
